# Visualizing Software Architecture
Having a standard way of visualizing software architecture is hugely beneficial, and it is very well summed up at [c4model.com] (https://c4model.com/)

Keeping the C4 Model upto date is one of the most important guideline that we need to follow, often time a diagram is created during the planning stage and then is left incomplete, and hence is not used while onboarding new team members or explaining the architecture to anyone. Having the models updated, allows it to be a single point of truth for the entire duration of the project to which everyone (devs, clietns) can have access to.

[How to use Structurizr to create C4 Models] (https://gitlab.com/smarter-codes/guidelines/software-engineering/architecture-design/c4-model/-/issues/1)
